# 程序配置


## 为什么需要剥离程序配置

![alt text](image.png)

## 配置定义

```go
func Default() *Config {
	return &Config{
		Application: &application{
			Host: "127.0.0.1",
			Port: 8080,
		},
		MySQL: &mySQL{
			Host:     "127.0.0.1",
			Port:     3306,
			DB:       "test",
			Username: "root",
			Password: "123456",
			Debug:    true,
		},
	}
}

// 这歌对象就是程序配置
type Config struct {
	Application *application `toml:"app" yaml:"app" json:"app"`
	MySQL       *mySQL       `toml:"mysql" yaml:"mysql" json:"mysql"`
}

// 应用服务
type application struct {
	Host string `toml:"host" yaml:"host" json:"host" env:"HOST"`
	Port int    `toml:"port" yaml:"port" json:"port" env:"PORT"`
}

// db对象也是一个单列模式
type mySQL struct {
	Host     string `json:"host" yaml:"host" toml:"host" env:"DATASOURCE_HOST"`
	Port     int    `json:"port" yaml:"port" toml:"port" env:"DATASOURCE_PORT"`
	DB       string `json:"database" yaml:"database" toml:"database" env:"DATASOURCE_DB"`
	Username string `json:"username" yaml:"username" toml:"username" env:"DATASOURCE_USERNAME"`
	Password string `json:"password" yaml:"password" toml:"password" env:"DATASOURCE_PASSWORD"`
	Debug    bool   `json:"debug" yaml:"debug" toml:"debug" env:"DATASOURCE_DEBUG"`
}
```

## yaml

+ [yaml](https://github.com/go-yaml/yaml)

```go
func main() {
	path := os.Getenv("CONFIG_PATH")
	if path == "" {
		path = "application.yaml"
	}
	content, err := os.ReadFile(path)
	if err != nil {
		panic(err)
	}

	config := Default()
	yaml.Unmarshal(content, config)

	dj, _ := json.Marshal(config)
	fmt.Println(string(dj))
}
```


## env

+ [env](https://github.com/caarlos0/env)

```go
func main() {
	config := Default()
	env.Parse(config)

	dj, _ := json.Marshal(config)
	fmt.Println(string(dj))
}
```
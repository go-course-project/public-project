# Gin Server交互示例



## 演示

1. 启动服务
```sh
$ go run main.go 
[GIN-debug] [WARNING] Creating an Engine instance with the Logger and Recovery middleware already attached.

[GIN-debug] [WARNING] Running in "debug" mode. Switch to "release" mode in production.
 - using env:   export GIN_MODE=release
 - using code:  gin.SetMode(gin.ReleaseMode)

[GIN-debug] GET    /hello                    --> main.main.func1 (3 handlers)
[GIN-debug] [WARNING] You trusted all proxies, this is NOT safe. We recommend you to set a value.
Please check https://pkg.go.dev/github.com/gin-gonic/gin#readme-don-t-trust-all-proxies for details.
[GIN-debug] Listening and serving HTTP on :8080
[GIN] 2024/08/25 - 11:51:14 | 404 |         750ns |             ::1 | GET      "/"
[GIN] 2024/08/25 - 11:51:21 | 200 |      58.666µs |             ::1 | GET      "/hello"
```

2. 验证
```sh
$ curl localhost:8080/hello
Hello, World!                      
```


## 流程示意图


```
+-----------------+                +-----------------+
|    客户端       |                |    服务器       |
| (浏览器或其他   |                | (运行 Gin 框架) |
|    HTTP 客户端) |                |                 |
+-----------------+                +-----------------+
        |                                   |
        |  1. 发送 HTTP GET 请求            |
        |---------------------------------->|
        |                                   |
        |                                   |
        |                                   |
        |                                   |
        |  2. 处理请求并生成响应           |
        |                                   |
        |                                   |
        |<----------------------------------|
        |                                   |
        |  3. 接收 HTTP 响应                |
        |                                   |
        +-----------------------------------+
```


## HTTP 协议报文示例 
 
1. 客户端发送的 HTTP GET 请求
```http
GET /hello HTTP/1.1
Host: localhost:8080
User-Agent: curl/7.64.1
Accept: */*
```

- **请求行**： GET /hello HTTP/1.1  表示请求方法为 GET，请求的资源为  /hello ，使用的 HTTP 版本为 1.1。 
- **请求头**： 
  -  Host ：指定请求的主机和端口。 
  -  User-Agent ：客户端的用户代理信息。 
  -  Accept ：客户端可以接受的内容类型。 
 
2. 服务器返回的 HTTP 响应
```http
HTTP/1.1 200 OK
Content-Type: text/plain; charset=utf-8
Content-Length: 13

Hello, World!
```

- **状态行**： HTTP/1.1 200 OK  表示使用的 HTTP 版本为 1.1，状态码为 200，状态描述为 OK。 
- **响应头**： 
  -  Content-Type ：指定响应体的内容类型为纯文本，字符集为 UTF-8。 
  -  Content-Length ：指定响应体的长度为 13 字节。 
- **响应体**： Hello, World!  是实际返回给客户端的内容。 
 
以上示例展示了一个简单的 HTTP 请求和响应过程，体现了客户端与服务器之间的交互。


## 请求处理

- **URL路径参数**：在URL路径中标记参数，如  /user/:id ，并在旁边标注获取方法  c.Param("id") 。
```go
router.GET("/book/:id", func(c *gin.Context) {
       id := c.Param("id") // 获取路径参数
       c.JSON(200, gin.H{"id": id})
   })
```

2. **获取URL Query参数**: 在URL中展示查询字符串，如  /search?q=value ，标注获取方法  c.Query("q")
```go
router.GET("/book", func(c *gin.Context) {
       query := c.Query("q") // 获取查询参数
       c.JSON(200, gin.H{"query": query})
   })
```



3. **获取Header参数**: 展示HTTP请求头部，标注获取方法  c.GetHeader("Authorization") 
```go
router.GET("/header", func(c *gin.Context) {
       headerValue := c.GetHeader("Authorization") // 获取Header参数
       c.JSON(200, gin.H{"Authorization": headerValue})
   })
```



4. **获取Body参数**: 展示一个JSON示例，如  { "name": "John" } ，标注获取方法  c.ShouldBindJSON(&user)
```go
router.POST("/book", func(c *gin.Context) {
       var book Book // 假设User是一个结构体
       if err := c.ShouldBindJSON(&book); err != nil { // 获取Body参数
           c.JSON(400, gin.H{"error": err.Error()})
           return
       }
       c.JSON(200, gin.H{"book": book})
   })
```

## 响应处理

1.  响应成功
```go
c.JSON(200, gin.H{
    "status":  "success",
    "message": "操作成功",
    "data":    map[string]interface{}{"id": 1, "name": "张三"},
})
```

```json
{
    "status": "success",
    "message": "操作成功",
    "data": {
        "id": 1,
        "name": "张三"
    }
}
```


2. 响应失败

```go
c.JSON(400, gin.H{
    "status":  "error",
    "message": "请求参数错误",
})
```

```json
{
    "status": "error",
    "message": "请求参数错误"
}
```
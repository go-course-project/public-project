# 基于Book Api的简单示例

## 环境准备

+ Go: go 1.22
+ IDE: VsCdoe
    + Go语言插件: [语法支持](https://marketplace.visualstudio.com/items?itemName=golang.Go)
    + MySQL插件: [UI操作MySQL](https://marketplace.visualstudio.com/items?itemName=cweijan.vscode-mysql-client2)

安装MySQL: 
```sh
docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 -d mysql:8.0 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
```



初始化项目
```go
go mod init "gitlab.com/go-course-project/public-project/book_api"
```
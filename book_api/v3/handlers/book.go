package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/go-course-project/public-project/book_api/v3/config"
	"gitlab.com/go-course-project/public-project/book_api/v3/models"
)

var BookHandler = &bookHanlder{}

type bookHanlder struct {
}

func (h *bookHanlder) Registry(r gin.IRouter) {
	r.POST("/books", h.CreateBook)
	r.GET("/books", h.ListBook)
	r.GET("/books/:id", h.GetBook)
	r.PUT("/books/:id", h.UpdateBook)
	r.DELETE("/books/:id", h.DeleteBook)
}

// 创建书籍
func (h *bookHanlder) CreateBook(c *gin.Context) {
	var book models.Book
	if err := c.ShouldBindJSON(&book); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	config.C().MySQL.GetDB().Create(&book)
	c.JSON(http.StatusCreated, book)
}

// 获取所有书籍
func (h *bookHanlder) ListBook(c *gin.Context) {
	var books []models.Book
	config.C().MySQL.GetDB().Find(&books)
	c.JSON(http.StatusOK, books)
}

// 根据 ID 获取书籍
func (h *bookHanlder) GetBook(c *gin.Context) {
	var book models.Book
	id := c.Param("id")

	// 使用日志模块打印日志
	config.C().Log.Logger().Debug().Msgf("book id: %s", id)

	if err := config.C().MySQL.GetDB().First(&book, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Book not found"})
		return
	}
	c.JSON(http.StatusOK, book)
}

// 更新书籍
func (h *bookHanlder) UpdateBook(c *gin.Context) {
	var book models.Book
	id := c.Param("id")
	if err := config.C().MySQL.GetDB().First(&book, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Book not found"})
		return
	}

	if err := c.ShouldBindJSON(&book); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	config.C().MySQL.GetDB().Save(&book)
	c.JSON(http.StatusOK, book)
}

// 删除书籍
func (h *bookHanlder) DeleteBook(c *gin.Context) {
	id := c.Param("id")
	if err := config.C().MySQL.GetDB().Delete(&models.Book{}, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Book not found"})
		return
	}
	c.JSON(http.StatusNoContent, nil)
}

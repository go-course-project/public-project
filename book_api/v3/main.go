package main

import (
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/go-course-project/public-project/book_api/v3/config"
	"gitlab.com/go-course-project/public-project/book_api/v3/handlers"
)

func main() {
	// 加载配置
	path := os.Getenv("CONFIG_PATH")
	if path == "" {
		path = "application.yaml"
	}
	config.LoadConfigFromYaml(path)

	r := gin.Default()
	handlers.BookHandler.Registry(r)

	ac := config.C().Application
	r.Run(fmt.Sprintf("%s:%d", ac.Host, ac.Port)) // 启动服务
}

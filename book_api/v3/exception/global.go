package exception

import (
	"fmt"
)

func ErrServerInternal(format string, a ...any) *ApiException {
	return &ApiException{
		Code:    CODE_SERVER_ERROR,
		Message: fmt.Sprintf(format, a...),
	}
}

func ErrNotFound(format string, a ...any) *ApiException {
	return &ApiException{
		Code:    CODE_NOT_FOUND,
		Message: fmt.Sprintf(format, a...),
	}
}

func ErrValidateFailed(format string, a ...any) *ApiException {
	return &ApiException{
		Code:    CODE_PARAM_INVALIDATE,
		Message: fmt.Sprintf(format, a...),
	}
}

package exception

const (
	CODE_SERVER_ERROR     = 5000
	CODE_NOT_FOUND        = 404
	CODE_PARAM_INVALIDATE = 400
)

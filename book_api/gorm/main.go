package main

import (
	"encoding/json"
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// Book 结构体定义
type Book struct {
	ID     uint    `json:"id" gorm:"primaryKey"`
	Title  string  `json:"title"`
	Author string  `json:"author"`
	Price  float64 `json:"price"`
}

// 初始化数据库
func setupDatabase() *gorm.DB {
	dsn := "root:123456@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	db.AutoMigrate(&Book{}) // 自动迁移
	return db
}

func main() {
	db := setupDatabase()

	// 创建书籍
	book := &Book{
		Title:  "will go",
		Author: "will",
		Price:  9.9,
	}
	db.Create(&book)

	// 获取所有书籍
	var books []Book
	db.Find(&books)

	// 打印数据
	dj, _ := json.Marshal(books)
	fmt.Println(string(dj))
}

# ORM

![orm](./orm.png)


## 演示

```sh
go run main.go 
[{"id":1,"title":"will go","author":"will","price":9.9}]
```

## 对象(Object)

```go
// Book 结构体定义
type Book struct {
	ID     uint    `json:"id" gorm:"primaryKey"`
	Title  string  `json:"title"`
	Author string  `json:"author"`
	Price  float64 `json:"price"`
}
```

## 数据

![mysql_data](./data.png)

## gorm CRUD

```go
db.Create(&book)
db.Find(&books)
db.First(&book, id)
db.Save(&book)
db.Delete(&Book{}, id)
```
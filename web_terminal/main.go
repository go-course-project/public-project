package main

import (
	"embed"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"github.com/infraboard/mcube/v2/http/response"
	"gitlab.com/go-course-project/public-project/web_terminal/k8s"
	"gitlab.com/go-course-project/public-project/web_terminal/terminal"
)

var (
	upgrader = websocket.Upgrader{
		HandshakeTimeout: 60 * time.Second,
		ReadBufferSize:   8192,
		WriteBufferSize:  8192,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
)

//go:embed ui
var ui embed.FS

func main() {
	http.HandleFunc("/ws/pod/terminal/log", func(w http.ResponseWriter, r *http.Request) {
		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			response.Failed(w, err)
			return
		}

		// 基于Websocket 构造Terminal对象
		term := terminal.NewWebSocketTerminal(ws)

		// 获取K8s 封装客户端
		kubeConf := k8s.MustReadContentFile("k8s/kube_config.yml")
		k8sClient, err := k8s.NewClient(kubeConf)
		if err != nil {
			term.Failed(err)
			return
		}

		// 读取用户通过Terminal传递过来的请求, 这个是建立WebSocket通信后
		// 通过 socket 发送的请求, 用于指定获取那个Pod的日志
		// 如果有认证信息, 也可以在这里一起定义
		req := k8s.NewWatchConainterLogRequest()
		if err = term.ReadReq(req); err != nil {
			term.Failed(err)
			return
		}

		// 获取 Pod 日志
		podReader, err := k8sClient.WatchConainterLog(r.Context(), req)
		if err != nil {
			term.Failed(err)
			return
		}

		// 读取出来的数据流 copy到term, term 的 Write 实现把数据 发送给用户
		_, err = io.Copy(term, podReader)
		if err != nil {
			term.Failed(err)
			return
		}
	})

	http.HandleFunc("/ws/pod/terminal/login", func(w http.ResponseWriter, r *http.Request) {
		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			response.Failed(w, err)
			return
		}

		// 基于Websocket 构造Terminal对象
		term := terminal.NewWebSocketTerminal(ws)

		// 获取K8s 封装客户端
		kubeConf := k8s.MustReadContentFile("k8s/kube_config.yml")
		k8sClient, err := k8s.NewClient(kubeConf)
		if err != nil {
			term.Failed(err)
			return
		}

		// 获取K8s 封装客户端
		req := k8s.NewLoginContainerRequest(term)
		if err = term.ReadReq(req); err != nil {
			term.Failed(err)
			return
		}

		// 登录容器
		err = k8sClient.LoginContainer(r.Context(), req)
		if err != nil {
			term.Failed(err)
			return
		}
	})

	web, _ := fs.Sub(ui, "ui")
	// ui/index --->  cd ui --> root --> ui/
	http.Handle("/", http.FileServer(http.FS(web)))

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println(err)
	}
}

package main

import (
	"github.com/infraboard/mcube/v2/ioc/server/cmd"

	_ "github.com/infraboard/mcube/v2/ioc/apps/apidoc/restful"
	_ "gitlab.com/go-course-project/public-project/webhook/apps"
)

func main() {
	cmd.Start()
}

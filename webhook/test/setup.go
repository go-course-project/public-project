package test

import (
	_ "github.com/infraboard/mcube/v2/ioc/apps/apidoc/restful"
	_ "gitlab.com/go-course-project/public-project/webhook/apps"

	"github.com/infraboard/mcube/v2/ioc"
)

func DevelopmentSetup() {
	ioc.DevelopmentSetup()
}

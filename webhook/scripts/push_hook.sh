#!/bin/bash

# 环境变量
echo "PROJECT_PATH: $PROJECT_PATH"
echo "PROJECT_LANGUAGE: $PROJECT_LANGUAGE"
echo "GIT_SSH_URL: $GIT_SSH_URL"
echo "GIT_BRANCH: $GIT_BRANCH"
echo "GIT_COMMIT: $GIT_COMMIT"
echo "DRY_RUN: $DRY_RUN"

if [ "$DRY_RUN" = "true" ]; then
    echo "DryRun 模式"
    exit 0
fi

# 判断变量是否以 release 开头
if [[ "$GIT_BRANCH" != release* ]]; then
    exit 1
fi

# 清理目录
[ -d "${PROJECT_PATH}" ] && { rm -rf "${PROJECT_PATH}"; echo "已删除目录: ${PROJECT_PATH}"; }
mkdir -pv ${PROJECT_PATH}

# Clone 代码
GIT_SSH_COMMAND='ssh -o StrictHostKeyChecking=no' git clone ${GIT_SSH_URL} ${PROJECT_PATH} --branch=${GIT_BRANCH} --single-branch && cd ${PROJECT_PATH} && git reset --hard ${GIT_COMMIT}

# 构建
mvn clean package

# 部署
JAR_PATH="target/thj-1.0-SNAPSHOT.jar"
APP_NAME=$(echo "$PROJECT_PATH" | awk -F/ '{print $NF}')
# 查找正在运行的进程
PID=$(ps -ef | grep "$JAR_PATH" | grep -v grep | awk '{print $2}')

if [ -n "$PID" ]; then
    echo "找到正在运行的进程，PID: $PID，正在重启..."
    # 杀死进程
    kill "$PID"

    # 等待进程完全结束
    sleep 2

    # 如果进程没有正常结束，强制杀死
    if ps -p "$PID" > /dev/null; then
        echo "进程未正常结束，强制杀死..."
        kill -9 "$PID"
    fi
else
    echo "未找到正在运行的进程。"
fi

# 启动 JAR 包
echo "启动 JAR 包..."
nohup java -jar "$JAR_PATH" -DAppName="$APP_NAME" -DGitBranch="$GIT_BRANCH" -DGitCommit="$GIT_COMMIT" > output.log 2>&1 &

echo "JAR 包已启动。"
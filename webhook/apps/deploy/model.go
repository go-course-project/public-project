package deploy

import (
	"fmt"
	"os"
	"path"
	"time"

	"github.com/google/uuid"
	"github.com/infraboard/mcube/v2/tools/pretty"
)

func NewDeploy(req *CreateDeployRequest) *Deploy {
	if req.Id == "" {
		req.Id = uuid.NewMD5(uuid.Nil, []byte(fmt.Sprintf("%s/%s/%s", req.Project, req.Branch, req.Commit))).String()
	}
	return &Deploy{
		CreateDeployRequest: req,
		Status:              NewStatus(),
	}
}

type Deploy struct {
	*CreateDeployRequest
	Status       *Status       `json:"status" gorm:"embedded;embeddedPrefix:status_"`
	ProcessState *ProcessState `json:"process_state" gorm:"embedded;embeddedPrefix:process_state_"`
}

func (d *Deploy) TableName() string {
	return "deploys"
}

func (d *Deploy) String() string {
	return pretty.ToJSON(d)
}

func NewCreateDeployRequest() *CreateDeployRequest {
	return &CreateDeployRequest{
		CreatedAt: time.Now().Unix(),
	}
}

type CreateDeployRequest struct {
	// 部署的Id
	Id string `json:"id" gorm:"column:id;primaryKey"`
	// 创建时间
	CreatedAt int64 `json:"created_at" gorm:"column:created_at"`
	// 触发事件的名称
	TriggerHook string `json:"trigger_hook" gorm:"column:trigger_hook"`
	// 事件的具体数据
	HookDetail string `json:"hook_detail" gorm:"column:hook_detail"`
	// 部署描述
	Description string `json:"description" gorm:"column:description"`
	// 项目名称
	Project string `json:"project" gorm:"column:project"`
	// 项目语言
	Language string `json:"language" gorm:"column:language"`
	// 项目地址
	GitSSHURL string `json:"git_ssh_url" gorm:"column:git_ssh_url"`
	// 部署分支
	Branch string `json:"branch" gorm:"column:branch"`
	// 部署Commit
	Commit string `json:"commit" gorm:"column:commit"`
	// 部署脚本, 默认{TriggerHook}.sh
	DeployScript string `json:"deploy_script" gorm:"column:deploy_script"`
	// 应用日志
	AppLog string `json:"app_log" gorm:"column:app_log"`
	// 尝试运行
	DryRun bool `json:"dry_run" gorm:"column:dry_run"`
}

func (h *CreateDeployRequest) WithHookDetail(detail string) *CreateDeployRequest {
	h.HookDetail = detail
	return h
}

func (h *CreateDeployRequest) Env() []string {
	env := os.Environ() // 获取当前环境变量
	env = append(env,
		"PROJECT_PATH="+h.ProjectPath(),
		"PROJECT_LANGUAGE="+h.Language,
		"GIT_SSH_URL="+h.GitSSHURL,
		"GIT_BRANCH="+h.Branch,
		"GIT_COMMIT="+h.Commit,
		"APP_LOG="+h.AppLog,
		"DRY_RUN="+fmt.Sprintf("%t", h.DryRun),
	)
	return env
}

func (h *CreateDeployRequest) ProjectPath() string {
	return path.Join("projects", h.Project)
}

func NewStatus() *Status {
	return &Status{}
}

// 部署脚本执行状态
type Status struct {
	ScriptLog string `json:"script_log" gorm:"column:script_log"`
	Message   string `json:"message" gorm:"column:message"`
	Success   bool   `json:"success" gorm:"column:success"`
}

// 进程状态
type ProcessState struct {
	Pid      int64  `json:"pid" gorm:"column:pid"`
	Detail   string `json:"detail" gorm:"column:detail"`
	ExitCode *int64 `json:"exit_code" gorm:"column:exit_code"`
	Success  bool   `json:"success" gorm:"column:success"`
}

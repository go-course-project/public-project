package api

import (
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/infraboard/mcube/v2/ioc"
	"github.com/infraboard/mcube/v2/ioc/config/gorestful"
	"github.com/infraboard/mcube/v2/types"
	"gitlab.com/go-course-project/public-project/webhook/apps/deploy"
)

func init() {
	ioc.Api().Registry(&DeployApiHandler{})
}

type DeployApiHandler struct {
	ioc.ObjectImpl
}

func (h *DeployApiHandler) Name() string {
	return deploy.AppName
}

func (h *DeployApiHandler) Init() error {
	tags := []string{"部署"}

	ws := gorestful.ObjectRouter(h)
	ws.Route(ws.GET("/").To(h.QueryDeploy).
		Doc("部署列表").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(deploy.QueryDeployRequest{}).
		Writes(types.Set[*deploy.Deploy]{}))

	return nil
}

package api

import (
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/v2/http/restful/response"
	"gitlab.com/go-course-project/public-project/webhook/apps/deploy"
)

func (h *DeployApiHandler) QueryDeploy(r *restful.Request, w *restful.Response) {
	req := deploy.NewQueryDeployRequest()

	set, err := deploy.GetService().QueryDeploy(r.Request.Context(), req)
	if err != nil {
		response.Failed(w, err)
		return
	}
	response.Success(w, set)
}

package impl

import (
	"context"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path"
	"path/filepath"

	"github.com/infraboard/mcube/v2/types"
	"gitlab.com/go-course-project/public-project/webhook/apps/deploy"
)

func (i *impl) CreateDeploy(ctx context.Context, in *deploy.CreateDeployRequest) (*deploy.Deploy, error) {
	ins := deploy.NewDeploy(in)

	// 创建一个日志文件
	ins.Status.ScriptLog = path.Join(i.LogDir, in.TriggerHook, in.Branch, in.Commit+".log")
	logFile, err := i.OpenLogFile(ins.Status.ScriptLog)
	if err != nil {
		return nil, fmt.Errorf("无法打开日志文件: %v", err)
	}
	defer logFile.Close()

	// 调用脚本
	cmd := exec.Command("/bin/bash", path.Join(i.ScriptDir, ins.DeployScript))
	cmd.Stdout = logFile
	cmd.Stderr = logFile
	cmd.Env = in.Env()
	err = cmd.Start()
	if err != nil {
		return nil, fmt.Errorf("执行脚本失败: %v", err)
	}

	if err := i.db.WithContext(ctx).Save(ins).Error; err != nil {
		return nil, err
	}

	return ins, nil
}

// EnsureDirExists 检查给定的文件夹路径是否存在，如果不存在，则创建它
func (h *impl) OpenLogFile(logFilePath string) (*os.File, error) {
	logDir := filepath.Dir(logFilePath)
	// 检查文件夹是否存在
	if _, err := os.Stat(logDir); os.IsNotExist(err) {
		// 如果不存在，则创建文件夹
		err := os.MkdirAll(logDir, os.ModePerm) // 使用 os.ModePerm 设置文件夹的权限
		if err != nil {
			return nil, fmt.Errorf("创建文件夹失败: %v", err)
		}
		h.log.Debug().Msgf("文件夹已创建: %s\n", logDir)
	}
	return os.OpenFile(logFilePath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
}

// 查询部署
func (i *impl) QueryDeploy(ctx context.Context, in *deploy.QueryDeployRequest) (*types.Set[*deploy.Deploy], error) {
	set := types.New[*deploy.Deploy]()
	query := i.db.WithContext(ctx).Model(&deploy.Deploy{})

	if err := query.Count(&set.Total).Error; err != nil {
		return nil, err
	}

	err := query.
		Order("created_at").
		Offset(int(in.ComputeOffset())).
		Limit(int(in.PageSize)).
		Find(&set.Items).
		Error
	if err != nil {
		return nil, err
	}
	return set, nil
}

// 查询部署日志
func (i *impl) WatchDeployLog(ctx context.Context, in *deploy.WatchDeployLogRequest) (io.Reader, error) {
	return nil, nil
}

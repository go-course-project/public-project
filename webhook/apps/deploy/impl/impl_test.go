package impl_test

import (
	"context"

	"gitlab.com/go-course-project/public-project/webhook/apps/deploy"
	"gitlab.com/go-course-project/public-project/webhook/test"
)

var (
	ctx  = context.Background()
	impl = deploy.GetService()
)

func init() {
	test.DevelopmentSetup()
}

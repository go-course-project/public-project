package impl_test

import (
	"testing"

	"gitlab.com/go-course-project/public-project/webhook/apps/deploy"
)

func TestQueryDeploy(t *testing.T) {
	set, err := impl.QueryDeploy(ctx, deploy.NewQueryDeployRequest())
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestCreateDeploy(t *testing.T) {
	req := deploy.NewCreateDeployRequest()
	req.DeployScript = "push_hook.sh"
	req.TriggerHook = "push_hook"
	req.Project = "cicd"
	req.Branch = "main"
	req.Commit = "xxx"
	req.DryRun = true
	set, err := impl.CreateDeploy(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

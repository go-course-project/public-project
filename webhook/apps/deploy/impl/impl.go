package impl

import (
	"github.com/infraboard/mcube/v2/ioc"
	"github.com/infraboard/mcube/v2/ioc/config/datasource"
	"github.com/infraboard/mcube/v2/ioc/config/log"
	"github.com/rs/zerolog"
	"gitlab.com/go-course-project/public-project/webhook/apps/deploy"
	"gorm.io/gorm"
)

func init() {
	ioc.Controller().Registry(&impl{
		ScriptDir: "scripts",
		LogDir:    "logs",
	})
}

type impl struct {
	ioc.ObjectImpl

	ScriptDir string `json:"script_dir" toml:"script_dir" env:"SCRIPT_DIR"`
	LogDir    string `json:"log_dir" toml:"log_dir" env:"LOG_DIR"`

	log *zerolog.Logger
	db  *gorm.DB
}

func (i *impl) Name() string {
	return deploy.AppName
}

func (i *impl) Init() error {
	i.log = log.Sub("deploy")
	i.db = datasource.DB()
	if err := i.db.AutoMigrate(&deploy.Deploy{}); err != nil {
		i.log.Error().Msgf("auto migrate error, %s", err)
	}
	return nil
}

package deploy

import (
	"context"
	"io"

	"github.com/infraboard/mcube/v2/http/request"
	"github.com/infraboard/mcube/v2/ioc"
	"github.com/infraboard/mcube/v2/types"
)

const (
	AppName = "deploy"
)

func GetService() Service {
	return ioc.Controller().Get(AppName).(Service)
}

type Service interface {
	// 创建部署
	CreateDeploy(context.Context, *CreateDeployRequest) (*Deploy, error)
	// 查询部署
	QueryDeploy(context.Context, *QueryDeployRequest) (*types.Set[*Deploy], error)
	// 查询部署日志
	WatchDeployLog(context.Context, *WatchDeployLogRequest) (io.Reader, error)
}

func NewQueryDeployRequest() *QueryDeployRequest {
	return &QueryDeployRequest{
		PageRequest: request.NewDefaultPageRequest(),
	}
}

type QueryDeployRequest struct {
	*request.PageRequest
}

type WatchDeployLogRequest struct {
}

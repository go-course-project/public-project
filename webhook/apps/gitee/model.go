package gitee

import (
	"fmt"
	"strings"

	"github.com/infraboard/mcube/v2/tools/pretty"
	"gitlab.com/go-course-project/public-project/webhook/apps/deploy"
)

func NewPushHook() *PushHook {
	return &PushHook{
		Repository: NewRepository(),
	}
}

type PushHook struct {
	// 推送的分支。eg：refs/heads/master
	Ref string `json:"ref"`
	// 钩子密码
	Password string `json:"password"`
	// 推送前分支的 commit id
	BeforeCommitId string `json:"before"`
	// 推送后分支的 commit id
	AfterCommintId string `json:"after"`
	//推送者的昵称。
	UserName string `json:"user_name"`
	// 仓库信息
	Repository *Repository `json:"repository"`
}

func (h *PushHook) CheckPassword(pass string) error {
	if h.Password != pass {
		return fmt.Errorf("hook 验证密码不正确")
	}
	return nil
}

func (h *PushHook) MakeCreateDeployRequest(hookName string) *deploy.CreateDeployRequest {
	req := deploy.NewCreateDeployRequest()
	req.Project = h.Repository.FullName
	req.Language = h.Repository.Language
	req.TriggerHook = hookName
	req.DeployScript = hookName + ".sh"
	req.GitSSHURL = h.Repository.GitSSHURL
	req.Branch = h.BranchName()
	req.Commit = h.AfterCommitShort()
	return req
}

func (h *PushHook) AfterCommitShort() string {
	if len(h.AfterCommintId) <= 8 {
		return h.AfterCommintId
	}
	return h.AfterCommintId[:8]
}

func (h *PushHook) String() string {
	return pretty.ToJSON(h)
}

func (h *PushHook) BranchName() string {
	list := strings.Split(h.Ref, "/")
	return list[len(list)-1]
}

func NewRepository() *Repository {
	return &Repository{}
}

type Repository struct {
	Private           bool   `json:"private"`
	StargazersCount   int    `json:"stargazers_count"`
	PushedAt          string `json:"pushed_at"`
	OpenIssuesCount   int    `json:"open_issues_count"`
	Description       string `json:"description"`
	CreatedAt         string `json:"created_at"`
	Language          string `json:"language"`
	GitHTTPURL        string `json:"git_http_url"`
	GitSSHURL         string `json:"git_ssh_url"`
	Path              string `json:"path"`
	HasWiki           bool   `json:"has_wiki"`
	UpdatedAt         string `json:"updated_at"`
	GitSVNURL         string `json:"git_svn_url"`
	SVNURL            string `json:"svn_url"`
	ID                int    `json:"id"`
	GitURL            string `json:"git_url"`
	HasPages          bool   `json:"has_pages"`
	Owner             Owner  `json:"owner"`
	PathWithNamespace string `json:"path_with_namespace"`
	SSHURL            string `json:"ssh_url"`
	HasIssues         bool   `json:"has_issues"`
	URL               string `json:"url"`
	Fork              bool   `json:"fork"`
	FullName          string `json:"full_name"`
	HTMLURL           string `json:"html_url"`
	CloneURL          string `json:"clone_url"`
	Name              string `json:"name"`
	Namespace         string `json:"namespace"`
	DefaultBranch     string `json:"default_branch"`
	WatchersCount     int    `json:"watchers_count"`
	NameWithNamespace string `json:"name_with_namespace"`
	ForksCount        int    `json:"forks_count"`
}

type Owner struct {
	AvatarURL string `json:"avatar_url"`
	UserName  string `json:"user_name"`
	HTMLURL   string `json:"html_url"`
	SiteAdmin bool   `json:"site_admin"`
	Name      string `json:"name"`
	ID        int    `json:"id"`
	Time      string `json:"time"`
	Login     string `json:"login"`
	Type      string `json:"type"`
	Email     string `json:"email"`
	URL       string `json:"url"`
	Username  string `json:"username"`
}

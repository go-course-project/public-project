package gitee

import (
	"encoding/json"
	"io"
	"os"
	"strings"

	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/v2/http/restful/response"
	"github.com/infraboard/mcube/v2/ioc"
	ioc_gorestful "github.com/infraboard/mcube/v2/ioc/config/gorestful"
	"github.com/infraboard/mcube/v2/ioc/config/log"
	"github.com/rs/zerolog"
	"gitlab.com/go-course-project/public-project/webhook/apps/deploy"
)

func init() {
	hn, _ := os.Hostname()
	ioc.Api().Registry(&WebHookHandler{
		Password: hn,
	})
}

type WebHookHandler struct {
	ioc.ObjectImpl
	log *zerolog.Logger

	// 默认主机名称
	Password string `json:"password" toml:"password" env:"PASSWORD"`
}

func (h *WebHookHandler) Init() error {
	tags := []string{"WebHook"}

	h.log = log.Sub("gitee")
	ws := ioc_gorestful.ObjectRouter(h)
	ws.Route(ws.POST("").To(h.HandleEvent).
		Doc("Gitee WebHook").
		Metadata(restfulspec.KeyOpenAPITags, tags))

	return nil
}

func (h *WebHookHandler) Name() string {
	return "gitee"
}

func (h *WebHookHandler) HandleEvent(r *restful.Request, w *restful.Response) {
	// 事件名称
	hookName := strings.ReplaceAll(strings.ToLower(strings.TrimSpace(r.HeaderParameter("X-Gitee-Event"))), " ", "_")
	h.log.Debug().Msgf("Gitee Event: [%s]", hookName)

	// 事件内容
	body, err := io.ReadAll(r.Request.Body)
	if err != nil {
		response.Failed(w, err)
		return
	}
	defer r.Request.Body.Close()
	h.log.Debug().Msgf(string(body))

	switch hookName {
	case "push_hook":
		e := NewPushHook()
		if err := json.Unmarshal(body, e); err != nil {
			response.Failed(w, err)
			return
		}
		h.log.Debug().Msgf("PushHook: %s", e)

		// 检查密码
		if err != e.CheckPassword(h.Password) {
			response.Failed(w, err)
			return
		}

		// 执行部署
		e.Password = "******"
		ins, err := deploy.GetService().CreateDeploy(r.Request.Context(), e.MakeCreateDeployRequest(hookName).WithHookDetail(e.String()))
		if err != nil {
			response.Failed(w, err)
			return
		}
		response.Success(w, ins)
	default:
		h.log.Debug().Msgf("hook name [%s] not match", hookName)
		response.Success(w, "ok")
		return
	}
}
